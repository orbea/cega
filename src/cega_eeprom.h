/*
 * Copyright (c) 2021-2022 Rupert Carmichael
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CEGA_EEPROM_H
#define CEGA_EEPROM_H

#define EEP93C46_MODE_STANDBY   0x00
#define EEP93C46_MODE_START     0x01
#define EEP93C46_MODE_OPCODE    0x02
#define EEP93C46_MODE_WRAL      0x03
#define EEP93C46_MODE_WRITE     0x04
#define EEP93C46_MODE_READ      0x05

typedef struct _eep93c46_t {
    uint8_t clk;
    uint8_t cs;
    uint8_t cyc;
    uint8_t dataout;
    uint8_t opcode;
    uint8_t addr;
    uint8_t mode;
    uint8_t wrenable;
    uint16_t datain;
} eep93c46_t;

void cega_eeprom_init_93c46(void);
uint8_t cega_eeprom_rd_93c46(void);
void cega_eeprom_wr_93c46(uint8_t*, uint8_t);
void eeprom_state_load_93c46(uint8_t*);
void eeprom_state_save_93c46(uint8_t*);

#endif
