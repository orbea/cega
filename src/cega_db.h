/*
 * Copyright (c) 2021 Rupert Carmichael
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CEGA_DB_H
#define CEGA_DB_H

#define SG_DB_TEREBI        0x00000001 // Terebi Oekaki Tablet

#define SMS_DB_PADDLE       0x00000001 // Paddle Controller
#define SMS_DB_PHASER       0x00000002 // Light Phaser
#define SMS_DB_SPORTS       0x00000004 // Sports Pad
#define SMS_DB_TMIRROR      0x00000100 // Tilemap Mirroring VDP quirk
#define SMS_DB_FRINT        0x00000200 // Boot with VDP Frame Interrupt enabled
#define SMS_DB_PHOFFSET     0x00000400 // Phaser X coordinates must be offset
#define SMS_DB_FMDISABLE    0x00000800 // FM Audio must be disabled

#define GG_DB_SMSMODE       0x00000001 // Game Gear games running in SMS Mode

#define DB_JP               0x40000000 // Force Japanese region
#define DB_PAL              0x80000000 // Force PAL region

uint32_t cega_db_process_gg(const char*);
uint32_t cega_db_process_md(const char*);
uint32_t cega_db_process_sg(const char*);
uint32_t cega_db_process_sms(const char*);

#endif
