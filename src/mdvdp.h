/*
 * Copyright (c) 2021-2022 Rupert Carmichael
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef MDVDP_H
#define MDVDP_H

#define MD_VDP_WIDTH_H40        320
#define MD_VDP_WIDTH_H32        256
#define MD_VDP_WIDTH            MD_VDP_WIDTH_H40
#define MD_VDP_HEIGHT           224
#define MD_VDP_HEIGHT_EXT       240
#define MD_VDP_HEIGHT_ILC       448
#define MD_VDP_HEIGHT_ILC_EXT   480
#define MD_VDP_SCANLINES        262
#define MD_VDP_SCANLINES_PAL    313

#define SIZE_MDCRAM             0x0040
#define SIZE_VSRAM              0x0028
#define SIZE_SPRCACHE           0x0140

typedef struct _mdvdp_t {
    uint8_t vram[SIZE_64K];
    uint16_t cram[SIZE_MDCRAM]; // Actually 9 bit values
    uint16_t vsram[SIZE_VSRAM]; // Actually 10 bit values
    uint8_t ctrl[32]; // VDP Control Registers
    uint16_t stat;
    uint16_t addr;
    uint8_t code;
    uint8_t wlatch; // Write Latch
    uint16_t line; // Line currently being drawn
    uint32_t mcyc; // Master Cycle in current scanline
    uint32_t dot; // Dot currently being drawn
    uint8_t lcount; // Line Interrupt Counter
    uint8_t rendering;
} mdvdp_t;

void mdvdp_set_buffer(uint32_t*);
void mdvdp_set_colramp(int ramp);
void mdvdp_set_region(uint8_t);
void mdvdp_set_size_callback(void (*)(int, int, int, int));

uint8_t mdvdp_rd08(uint32_t);
uint16_t mdvdp_rd16(uint32_t);
void mdvdp_wr08(uint32_t, uint8_t);
void mdvdp_wr16(uint32_t, uint16_t);

void mdvdp_init(void);
void mdvdp_reset(void);

int mdvdp_exec(void);

#endif
