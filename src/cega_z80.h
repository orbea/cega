/*
 * Copyright (c) 2021-2022 Rupert Carmichael
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CEGA_Z80_H
#define CEGA_Z80_H

void cega_z80_init(void);
void cega_z80_irq(uint8_t data);
void cega_z80_irq_clr(void);
uint8_t cega_z80_busack(void);
void cega_z80_busreq(uint8_t data);
void cega_z80_nmi(void);
void cega_z80_nmi_clr(void);
void cega_z80_reset(void);
uint8_t cega_z80_reset_stat(void);
void cega_z80_reset_line(uint8_t);
void cega_z80_delay(uint64_t);
uint32_t cega_z80_exec(void);

void cega_z80_state_load(uint8_t*);
void cega_z80_state_save(uint8_t*);

#endif
