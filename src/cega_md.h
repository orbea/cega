/*
 * Copyright (c) 2021-2022 Rupert Carmichael
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CEGA_MD_H
#define CEGA_MD_H

#define SIZE_MDRAM SIZE_64K
#define SIZE_ZRAM SIZE_8K

#define MD_MAPPER_NONE      0 // No Mapper
#define MD_MAPPER_SSF       1 // Super Street Fighter II Mapper

typedef struct _md_sys_t {
    uint8_t ram[SIZE_MDRAM]; // Main System RAM
    uint8_t sram[SIZE_64K]; // SRAM
    uint8_t zram[SIZE_ZRAM]; // Z80 RAM
    uint8_t mpage[8]; // 8 512K Pages
    uint16_t bankreg; // Z80 Bank Register
    uint8_t version; // Version Register
    uint8_t region;
    uint8_t mapper; // Mapper Present
    uint8_t sram_type; // SRAM Type
    uint8_t sram_lock; // SRAM Lock
} md_sys_t;

uint8_t cega_md_m68k_rd08(uint32_t addr);
uint16_t cega_md_m68k_rd16(uint32_t addr);
void cega_md_m68k_wr08(uint32_t addr, uint8_t data);
void cega_md_m68k_wr16(uint32_t addr, uint16_t data);

void cega_md_init(void);
void cega_md_deinit(void);

void cega_md_reset(void);

int cega_md_sram_load(const char*);
int cega_md_sram_save(const char*);

void cega_md_set_region(uint8_t);
uint8_t cega_md_get_region(void);

void cega_mdexec(void);

void cega_md_vdpsync(void);

size_t cega_md_state_size(void);

void cega_md_state_load_raw(const void*);
int cega_md_state_load(const char*);

const void* cega_md_state_save_raw(void);
int cega_md_state_save(const char*);

#endif
