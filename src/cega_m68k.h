/*
 * Copyright (c) 2021-2022 Rupert Carmichael
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CEGA_M68K_H
#define CEGA_M68K_H

uint64_t cega_m68k_cyc_total(void);
void cega_m68k_delay(size_t);
void cega_m68k_irq(int);
void cega_m68k_init(void);
uint32_t cega_m68k_exec(void);
uint32_t cega_m68k_run(int);
void cega_m68k_reset(void);

#endif
