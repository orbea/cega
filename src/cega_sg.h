/*
 * Copyright (c) 2021-2022 Rupert Carmichael
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CEGA_SG_H
#define CEGA_SG_H

#define SIZE_SGRAM SIZE_1K

#define SG_MAPPER_RAM2K  1 // 2K RAM on Cartridge
#define SG_MAPPER_RAM8K  2 // 8K RAM on Cartridge
#define SG_MAPPER_TEREBI 3 // Terebi Oekaki
#define SG_MAPPER_TWMSXA 4 // Taiwan MSX Type A RAM Expansion (External)
#define SG_MAPPER_TWMSXB 5 // Taiwan MSX Type B RAM Expansion (External)

#define SG_TEREBI_X 0 // Terebi Oekaki X Axis
#define SG_TEREBI_Y 1 // Terebi Oekaki Y Axis
#define SG_TEREBI_P 2 // Terebi Oekaki Pressure

typedef struct _sg_sys_t {
    uint8_t ram[SIZE_SGRAM]; // System RAM
    uint8_t expram[SIZE_8K]; // Maximum 8K of Expansion RAM
    uint8_t ctrl[2]; // Controller Input state
    uint8_t tbaxis; // Terebi Oekaki Axis
} sg_sys_t;

void cega_sg_terebi_set_callback(uint8_t (*)(uint8_t));

void cega_sg_init(void);
void cega_sg_deinit(void);

void cega_sg_reset(void);

void cega_sg_set_mapper(uint16_t);

size_t cega_sg_state_size(void);

void cega_sg_state_load_raw(const void*);
int cega_sg_state_load(const char*);

const void* cega_sg_state_save_raw(void);
int cega_sg_state_save(const char*);

int cega_sg_sram_load(const char*);
int cega_sg_sram_save(const char*);

#endif
