/*
 * Copyright (c) 2021-2022 Rupert Carmichael
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CEGA_MIXER_H
#define CEGA_MIXER_H

void cega_mixer_deinit(void);
void cega_mixer_init(uint8_t);

void cega_mixer_get_cycs(uint8_t*, uint8_t*);
void cega_mixer_set_cycs(uint8_t, uint8_t);

void cega_mixer_set_buffer(int16_t*);
void cega_mixer_set_callback(void (*)(size_t));
void cega_mixer_set_rate(size_t);
void cega_mixer_set_region(uint8_t);
void cega_mixer_set_rsqual(uint8_t);
void cega_mixer_chips_enable(uint8_t, uint8_t);
void cega_mixer_chips_exec(void);
void cega_mixer_resamp(void);
void cega_mixer_resamp_md(size_t, size_t);

#endif
