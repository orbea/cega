/*
 * Copyright (c) 2021 Rupert Carmichael
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CEGA_SMS_IO_H
#define CEGA_SMS_IO_H

// SMS Input Types
#define SMS_INPUT_TYPE_NONE     0
#define SMS_INPUT_TYPE_PAD      1
#define SMS_INPUT_TYPE_PADDLE   2
#define SMS_INPUT_TYPE_PHASER   3
#define SMS_INPUT_TYPE_SPORTS   4

// SMS Control Pad Input Bits
#define SMS_INPUT_U         0x01
#define SMS_INPUT_D         0x02
#define SMS_INPUT_L         0x04
#define SMS_INPUT_R         0x08
#define SMS_INPUT_1         0x10
#define SMS_INPUT_2         0x20
#define SMS_INPUT_RESET     0x40
#define SMS_INPUT_PAUSE     0x80

// SMS Light Phaser Input Bits
#define SMS_INPUT_PHASER    0x10

// Game Gear Input Bits
#define GG_INPUT_U      0x01
#define GG_INPUT_D      0x02
#define GG_INPUT_L      0x04
#define GG_INPUT_R      0x08
#define GG_INPUT_1      0x10
#define GG_INPUT_2      0x20
#define GG_INPUT_START  0x80

uint8_t cega_sms_io_rd(uint8_t);
void cega_sms_io_set_port(int, int);
void cega_sms_io_init(void);

#endif
